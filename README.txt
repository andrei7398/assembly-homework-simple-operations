SFETCU IULIAN-ANDREI - 322CD - TEMA 1 IOCLA

Registre folosite:

eax ->	se introduce de fiecare data pe stiva, fie ca provine dintr-un numar citit din string
	sau rezultat in urma unei operatii
ebx ->	il folosesc ca un fel de 'FLAG' astfel:
    	-> daca ebx = 1 atunci la ultimul pas am introdus un numar rezultat dintr-o operatie
	   efectuata (push a fost realizat deja de operatie)
	-> daca ebx = 2 atunci la ultimul pas am citit un numar pozitiv din string (push)
	-> daca ebx = 3 atunci la ultimul pas am citit un numar negativ din string (negare & push)
	-> daca ebx = 5 atunci la ultimul pas am citit un numar negativ iar numarul citit curent e tot 
		      negativ (a fost rezolvarea la eroare)	
ecx ->	indexul
edx -> folosit pentru operatia de impartire

La fiecare pas:

Verific daca elementul de la indexul curent este spatiu
	In caz afirmativ:
		- cresc indexul
		- verific ce am avut la pasul trecut(numar pozitiv/ numar negativ/ operatie)
		- in functie de pasul trecut fac operatiile necesare(negare da/n, push da/nu)
		- citesc urmatorul caracter*

(*) pasii de la * incolo corespund in ambele cazuri

	In caz negativ:
		- citesc urmatorul caracter*
		- verific daca este '-'
			- daca da si elementul de pe pozitia urmatoare este cifra -> numar negativ (**)
			- daca da si elementul de pe pozitia urmatoare este spatiu(nu cifra) -> operatie
		- daca nu este '-', verific '+','*','/'
		- daca este vreunul din operatori sar la operatia respectiva iar apoi din nou la citire
		- daca nu este operator construiesc numarul
			- scad din fiecare caracter '0' (ASCII) -> cifra
			- inmultesc cu 10 pentru a schimba unitatea
			- (**) se neaga la final tot numarul