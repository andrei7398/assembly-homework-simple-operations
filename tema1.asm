%include "io.inc"

%define MAX_INPUT_SIZE 4096

section .bss
	expr: resb MAX_INPUT_SIZE

section .text
global CMAIN

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  SFETCU IULIAN-ANDREI 322CD  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CMAIN:
    mov ebp, esp; for correct debugging
    
    GET_STRING expr, MAX_INPUT_SIZE
    
    mov ecx, 0 ; our index
    
; ebx = 1 - operatie inainte de pasul curent
; ebx = 2 - numar pozitiv inainte de pasul curent
; ebx = 3 - numar negativ inainte de pasul curent    
    
atoi:
    xor eax, eax ; curatam eax

top:
    movzx esi, byte[expr + ecx] ; get a character

    cmp byte[expr + ecx],' '    ; numarul/operatia s-a incheiat
    jne next
    
    inc ecx         ; crestem index
    
    cmp ebx, 1      ; fara adaugare in stiva, operatia a adaugat deja rezultatul
    je skip
    
    cmp ebx, 2      ; adaugare in stiva, fara negare
    je skipnot
    
    not eax         ; negam
    add eax, 0x1    ; adaugam 1
    mov ebx, 5      ; cazul 2 numere negative consecutive - for debug

skipnot:
    push eax        ; numar pozitiv/negativ - introducem in stiva

skip:
    jmp atoi

next:
    cmp byte[expr + ecx],'-'    ; daca '-' atunci numar negativ sau operatie de scadere
    jne another
    cmp byte[expr + ecx + 1],'0' ; daca '-' si urmatoarea cifra, rezulta numar negativ
    jb opmin
    cmp byte[expr + ecx + 1],'9' ; daca '-' si urmatoarea cifra, rezulta numar negativ
    ja opmin
    cmp byte[expr + ecx + 1],'' ; sigur operatie de scadere (ultima)
    je opmin

negative:
    inc ecx     ; crestem index
    mov ebx, 3  ; numar negativ
    jmp top

another:
    cmp esi, '+'    ; sarim la operatia de adunare
    je oppls
    
    cmp esi, '/'    ; sarim la operatia de scadere
    je opidiv
    
    cmp esi, '*'    ; sarim la operatia de inmultire
    je opimul
    
    cmp esi, 0      ; null terminator - ne oprim
    je done
    
    inc ecx         ; crestem indexul
    
    sub esi, '0'    ; convertim caracterul in numar
    imul eax, 10    ; multiplicam cu 10, in caz ca numarul mai are cifre
    add eax, esi    ; adaugam cifra curenta
    
    cmp ebx, 3      ; minus detectat, deci numar negativ -> lasam ebx 3
    je skip3
    
    mov ebx, 2

; numar negativ  
skip3:
    jmp top         ; pana cand terminam stringul
    
; operatia de adunare
oppls:        
        pop eax
        pop ebx
        add eax, ebx    ; adunare - ultimele 2 elemente din stiva
        
        push eax    ; adaugam in stiva rezultatul
        mov ebx, 1  ; operatie - stiva deja acutalizata
        inc ecx     ; crestem index
        
        jmp atoi

; operatia de scadere                
opmin:        
        pop ebx
        pop eax
        sub eax, ebx    ; scadere - ultimele 2 elemente din stiva
        
        push eax    ; adaugam in stiva rezultatul
        mov ebx, 1  ; operatie - stiva deja acutalizata
        inc ecx     ; crestem index
        
        jmp atoi

; operatia de impartire                
opidiv:       
        xor edx, edx    ; curatam registrii
        pop ebx
        pop eax
        cdq
        idiv ebx    ; impartire - ultimele 2 elemente din stiva
        
        push eax;   ; adaugam in stiva rezultatul
        mov ebx, 1  ; operatie - stiva deja acutalizata
        inc ecx     ; crestem index
        
        jmp atoi

; operatia de inmultire              
opimul:        
        pop eax
        pop ebx
        imul eax, ebx   ; inmultire - ultimele 2 elemente din stiva
        
        push eax    ; adaugam in stiva rezultatul
        mov ebx, 1  ; operatie - stiva deja acutalizata
        inc ecx     ; crestem index
        
        jmp atoi

; am terminat stringul
done:
    xor eax,eax     ; curatam registrul in care punem rezultatul final
    pop eax         ; extragem din stiva singurul element ramas (rezultatul final)
 
    PRINT_DEC 4, eax    ; afisam rezultatul

    xor eax, eax    ; curatam registrii
    ret